# POSTGRES #

- [Basics](https://bitbucket.org/barragoon/postgres/pull-requests/3)
- [Config](https://bitbucket.org/barragoon/postgres/pull-requests/2)
- [Indexes](https://bitbucket.org/barragoon/postgres/pull-requests/4)
- [Index types](https://bitbucket.org/barragoon/postgres/pull-requests/5)
- [Operator Class](https://bitbucket.org/barragoon/postgres/pull-requests/6)
